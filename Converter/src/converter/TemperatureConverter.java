/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 * Converts temperatures.Currently supports Kelvin, Celsius, Fahrenheit.
 * @author Alice
 */
public class TemperatureConverter {
    
    /**
     * Converts a temperature value from inputUnit to outputUnit.
     * @param inputValue Value to convert.
     * @param inputUnit Unit of inputValue.
     * @param outputUnit Desired output unit.
     * @return Value that has been converted from inputUnit to outputUnit.
     * @throws UnitMismatchException if invalid unit is used.
     */
    public static double Convert(double inputValue, Units inputUnit, Units outputUnit) throws UnitMismatchException{
        switch(inputUnit){
            case KELVIN:
                return ConvertFromKelvin(inputValue, outputUnit);
            case CELSIUS:
                return ConvertFromCelsius(inputValue, outputUnit);
            case FAHRENHEIT:
                return ConvertFromFahrenheit(inputValue, outputUnit);
            default: throw new UnitMismatchException("The input unit is not supported");
        }
    }
    
    public static double ConvertKelvinToCelsius(double value){
        return value - 273.15;
    }
    
    public static double ConvertKelvinToFahrenheit(double value){
        return ((value - 273.15) * 9 / 5) + 32;
    }
    
    public static double ConvertCelsiusToKelvin(double value){
        return value + 273.15;
    }
    
    public static double ConvertCelsiusToFahrenheit(double value){
        return (value * 9 / 5) + 32;
    }
    
    public static double ConvertFahrenheitToKelvin(double value){
        return ((value - 32) * 5 / 9) + 273.15;
    }
    
    public static double ConvertFahrenheitToCelsius(double value){
        return (value - 32) * 5 / 9;
    }
    
    /**
     * Converts from Kelvin to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromKelvin(double value, Units output) throws UnitMismatchException{
        switch(output){
            case KELVIN:
                return value;
            case CELSIUS:
                return ConvertKelvinToCelsius(value);
            case FAHRENHEIT:
                return ConvertKelvinToFahrenheit(value);
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
    
    /**
     * Converts from Celsius to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromCelsius(double value, Units output) throws UnitMismatchException{
        switch(output){
            case KELVIN:
                return ConvertCelsiusToKelvin(value);
            case CELSIUS:
                return value;
            case FAHRENHEIT:
                return ConvertCelsiusToFahrenheit(value);
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
    
    /**
     * Converts from Fahrenheit to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromFahrenheit(double value, Units output) throws UnitMismatchException{
        switch(output){
            case KELVIN:
                return ConvertFahrenheitToKelvin(value);
            case CELSIUS:
                return ConvertFahrenheitToCelsius(value);
            case FAHRENHEIT:
                return value;
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
}
