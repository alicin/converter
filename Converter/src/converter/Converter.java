/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 * General converter class.
 * @author Alice
 */
public class Converter {
    private enum UnitType{
        TEMPERATURE, VOLUME;
    }
    
    /**
     * Converts value from inputUnit to outputUnit.
     * @param value Value to convert.
     * @param inputUnit Unit of inputValue.
     * @param outputUnit Desired output unit.
     * @return Value that has been converted from inputUnit to outputUnit.
     * @throws UnitMismatchException if invalid unit is used, or if two incompatible units are entered (e.g. attempts to convert Kelvin to Litres).
     */
    public static double Convert(double value, Units inputUnit, Units outputUnit) throws UnitMismatchException{
        UnitType inputType;
        UnitType outputType;
        
        switch(inputUnit){
            case KELVIN:
            case CELSIUS:
            case FAHRENHEIT:
                inputType = UnitType.TEMPERATURE;
                break;
            case LITRE:
            case MILLILITRE:
            case US_GALLON:
                inputType = UnitType.VOLUME;
                break;
            default: throw new UnitMismatchException("The input unit is not supported");
        }
        
        switch(outputUnit){
            case KELVIN:
            case CELSIUS:
            case FAHRENHEIT:
                outputType = UnitType.TEMPERATURE;
                break;
            case LITRE:
            case MILLILITRE:
            case US_GALLON:
                outputType = UnitType.VOLUME;
                break;
            default: throw new UnitMismatchException("The output unit is not supported");
        }
        
        //if trying to convert across Temp/Volumes, dont allow it
        if(inputType != outputType){
            throw new UnitMismatchException("inputUnit and outputUnit are incompatible");
        }
        
        //since inputType == outputType
        switch(inputType){
            case TEMPERATURE:
                return TemperatureConverter.Convert(value, inputUnit, outputUnit);
            case VOLUME:
                return VolumeConverter.Convert(value, inputUnit, outputUnit);
            default: throw new UnitMismatchException("Specified unit type is not supported");
        }
    }
}
