/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 * To be used when the units used as parameters in the function do not match up.
 * @author Alice
 */
public class UnitMismatchException extends Exception {

    /**
     * Creates a new instance of <code>UnitMismatchException</code> without
     * detail message.
     */
    public UnitMismatchException() {
    }

    /**
     * Constructs an instance of <code>UnitMismatchException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UnitMismatchException(String msg) {
        super(msg);
    }
}
