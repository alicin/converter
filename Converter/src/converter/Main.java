/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import java.util.Scanner;

/**
 * Test program using the converter class.
 * @author Alice
 */
public class Main {
    
    static Scanner scanner = new Scanner(System.in);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean quit = false;
        int choice;
        
        do{
            System.out.println("Please select an option:");
            System.out.println("1. Show unit test results.");
            System.out.println("2. Use converter.");
            System.out.println("3. Quit.");
            
            choice = scanner.nextInt();
            
            switch(choice){
                case 1:
                    UnitTests.UnitTests();
                    break;
                case 2:
                    RunConverter();
                    break;
                case 3:
                    quit = true;
                    break;
                default: System.out.println("Invalid choice");
            }
        }while(!quit);
    }
    
    private static void RunConverter(){
        double value;
        int inputUnit;
        int outputUnit;
        
        System.out.println("Please enter the value you wish to convert and press enter.");
        value = scanner.nextDouble();
        
        System.out.println("Please enter a number to represent the units this value is in, from the following list.");
        PrintUnits();
        inputUnit = scanner.nextInt();
        
        System.out.println("Please enter a number to represent the units you want to conver to, from the following list.");
        PrintUnits();
        outputUnit = scanner.nextInt();
        
        if(inputUnit > 0 && inputUnit < 7 && outputUnit > 0 && outputUnit < 7){
            try{
                System.out.println("Converted value is: " + Converter.Convert(value, ConvertToUnit(inputUnit), ConvertToUnit(outputUnit)));
            }
            catch(UnitMismatchException e){
                System.out.println(e.getMessage());
            }
        }
        else{
            System.out.println("Invalid units numbers entered.");
        }
        
        System.out.println();
    }
    
    private static Units ConvertToUnit(int val){
        switch(val){
            case 1: return Units.KELVIN;
            case 2: return Units.CELSIUS;
            case 3: return Units.FAHRENHEIT;
            case 4: return Units.LITRE;
            case 5: return Units.MILLILITRE;
            default: return Units.US_GALLON;
        }
    }

    private static void PrintUnits(){
        System.out.println("1. Kelvin");
        System.out.println("2. Celsius");
        System.out.println("3. Fahrenheit");
        System.out.println("4. Litre");
        System.out.println("5. Millilitre");
        System.out.println("6. US Gallon");
    }
}
