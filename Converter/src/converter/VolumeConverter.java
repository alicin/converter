/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 * Converts volumes. Currently supports Millilitres, Litres, US Gallons.
 * @author Alice
 */
public class VolumeConverter {
    
    /**
     * Converts a volumetric value from inputUnit to outputUnit.
     * @param inputValue Value to convert.
     * @param inputUnit Unit of inputValue.
     * @param outputUnit Desired output unit.
     * @return Value that has been converted from inputUnit to outputUnit.
     * @throws UnitMismatchException if invalid unit is used.
     */
    public static double Convert(double inputValue, Units inputUnit, Units outputUnit) throws UnitMismatchException{
        switch(inputUnit){
            case MILLILITRE:
                return ConvertFromMillilitres(inputValue, outputUnit);
            case LITRE:
                return ConvertFromLitres(inputValue, outputUnit);
            case US_GALLON:
                return ConvertFromUSGallons(inputValue, outputUnit);
            default: throw new UnitMismatchException("The input unit is not supported");
        }
    }
    
    public static double MillilitresToLitres(double value){
        return value / 1000.0;
    }
    
    public static double MillilitresToUSGallons(double value){
        return value / 3785.411784;
    }
    
    public static double LitresToMillilitres(double value){
        return value * 1000.0;
    }
    
    public static double LitresToUSGallons(double value){
        return value / 3.785411784;
    }
    
    public static double USGallonsToMillilitres(double value){
        return value * 3785.411784;
    }
    
    public static double USGallonsToLitres(double value){
        return value * 3.785411784;
    }
    
    /**
     * Converts from Millilitres to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromMillilitres(double value, Units output) throws UnitMismatchException{
        switch(output){
            case MILLILITRE:
                return value;
            case LITRE:
                return MillilitresToLitres(value);
            case US_GALLON:
                return MillilitresToUSGallons(value);
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
    
    /**
     * Converts from Litres to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromLitres(double value, Units output) throws UnitMismatchException{
        switch(output){
            case MILLILITRE:
                return LitresToMillilitres(value);
            case LITRE:
                return value;
            case US_GALLON:
                return LitresToUSGallons(value);
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
    
    /**
     * Converts from US Gallons to specified unit.
     * @param value the value to convert.
     * @param output the unit to convert to.
     * @return the converted value.
     * @throws UnitMismatchException if invalid unit is used.
     */
    private static double ConvertFromUSGallons(double value, Units output) throws UnitMismatchException{
        switch(output){
            case MILLILITRE:
                return USGallonsToMillilitres(value);
            case LITRE:
                return USGallonsToLitres(value);
            case US_GALLON:
                return value;
            default: throw new UnitMismatchException("The output unit is not supported");
        }
    }
}
