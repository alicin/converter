/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import java.util.Arrays;

/**
 * This contains a collection of unit tests for the TemperatureConverter, VolumeConverter
 * @author Alice
 */
public class UnitTests {
    static public void UnitTests(){
        System.out.println("TemperatureConverter Tests:");
        TestTemperatureConverter();
        System.out.println();
        
        System.out.println("VolumeConveter Tests:");
        TestVolumeConverter();
        System.out.println();
        
        System.out.println("Test for units that don't match");
        TestMismatchedUnits();
        System.out.println();
    }
    
    static private void TestTemperatureConverter(){
        try{
            System.out.println("Celsius to Fahrenheit");
                //test both 0 point conversions
            System.out.println("Input 0 Celsius, should result in 32F:" + TemperatureConverter.Convert(0, Units.CELSIUS, Units.FAHRENHEIT));
            System.out.println("Input -17.7778 Celsius, should result in 0F:" + TemperatureConverter.Convert(-17.7778, Units.CELSIUS, Units.FAHRENHEIT));
                //test a non 0 point number
            System.out.println("Input 10 Celsius, should result in 50F: " + TemperatureConverter.Convert(10, Units.CELSIUS, Units.FAHRENHEIT));
                //test C-F equivalence point
            System.out.println("Input -40 Celsius, should result in -40F: " + TemperatureConverter.Convert(-40, Units.CELSIUS, Units.FAHRENHEIT));
            System.out.println();
            
            System.out.println("Celsius to Kelvin");
                //test both 0 point conversions
            System.out.println("Input 0 Celsius, should result in 273.15K: " + TemperatureConverter.Convert(0, Units.CELSIUS, Units.KELVIN));
            System.out.println("Input -273.15 Celsius, should result in 0K: " + TemperatureConverter.Convert(-273.15, Units.CELSIUS, Units.KELVIN));
                //test a non 0 point number
            System.out.println("Input 10 Celsius, should be 283.15K: " + TemperatureConverter.Convert(10, Units.CELSIUS, Units.KELVIN));
            System.out.println();
            
            System.out.println("Fahrenheit to Celsius");
                //test both 0 point conversions
            System.out.println("Input 0 Fahrenheit, should result in -17.7778C: " + TemperatureConverter.Convert(0, Units.FAHRENHEIT, Units.CELSIUS));
            System.out.println("Input 32 Fahrenheit, should result in 0C: " + TemperatureConverter.Convert(32, Units.FAHRENHEIT, Units.CELSIUS));
                //test a non 0 point number
            System.out.println("Input 10 Fahrenheit, should result in -12.2222C: " + TemperatureConverter.Convert(10, Units.FAHRENHEIT, Units.CELSIUS));
                //test test C-F equivalence point
            System.out.println("Input -40 Farenheit, should result in -40C: " + TemperatureConverter.Convert(-40, Units.FAHRENHEIT, Units.CELSIUS));
            System.out.println();
            
            System.out.println("Fahrenheit to Kelvin");
                //test both 0 point conversions
            System.out.println("Input 0 Fahrenheit, should result in 255.372K: " + TemperatureConverter.Convert(0, Units.FAHRENHEIT, Units.KELVIN));
            System.out.println("Input -459.67 Fahrenheit, should result in 0K: " + TemperatureConverter.Convert(-459.67, Units.FAHRENHEIT, Units.KELVIN));
                //test a non 0 point number
            System.out.println("Input 10 Fahrenheit, should result in 260.928K: " + TemperatureConverter.Convert(10, Units.FAHRENHEIT, Units.KELVIN));
            System.out.println();
            
            System.out.println("Kelvin to Celsius");
                //test both 0 point converstions
            System.out.println("Input 0 Kelvin, should result in -273.15C: " + TemperatureConverter.Convert(0, Units.KELVIN, Units.CELSIUS));
            System.out.println("Input 273.15 Kelvin, should result in 0C: " + TemperatureConverter.Convert(273.15, Units.KELVIN, Units.CELSIUS));
                //test a non 0 point number
            System.out.println("Input 10 Kelvin, should result in -263.15C: " + TemperatureConverter.Convert(10, Units.KELVIN, Units.CELSIUS));
            System.out.println();
            
            System.out.println("Kelvin to Fahrenheit");
                //test both 0 point converstions
            System.out.println("Input 0 Kelvin, should result in -459.67F: " + TemperatureConverter.Convert(0, Units.KELVIN, Units.FAHRENHEIT));
            System.out.println("Input 255.372 Kelvin, should result in 0F: " + TemperatureConverter.Convert(255.372, Units.KELVIN, Units.FAHRENHEIT));
                //test a non 0 point number
            System.out.println("Input 10 Kelvin, should result in -441.67F: " + TemperatureConverter.Convert(10, Units.KELVIN, Units.FAHRENHEIT));
            System.out.println();
        }
        catch(UnitMismatchException e){
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
    
    static private void TestVolumeConverter(){
        try{
            System.out.println("Litres to Millilitres");
                //test 0 points
            System.out.println("Input 0 Litres, should result in 0mL: " + VolumeConverter.Convert(0, Units.LITRE, Units.MILLILITRE));
                //test non 0 point number
            System.out.println("Input 10 Litres, should result in 10000mL: " + VolumeConverter.Convert(10, Units.LITRE, Units.MILLILITRE));
            System.out.println();
            
            System.out.println("Litres to US Gallons");
                //test 0 points
            System.out.println("Input 0 Litres, should result in 0US gal: " + VolumeConverter.Convert(0, Units.LITRE, Units.US_GALLON));
                //test non 0 point number
            System.out.println("Input 10 Litres, should result in 2.64172US gal: " + VolumeConverter.Convert(10, Units.LITRE, Units.US_GALLON));
            System.out.println();
            
            System.out.println("Millilitres to Litres");
                //test 0 points
            System.out.println("Input 0 Millilitres, should result in 0L: " + VolumeConverter.Convert(0, Units.MILLILITRE, Units.LITRE));
                //test non 0 point number
            System.out.println("Input 10 Millilitres, should result in 0.01L: " + VolumeConverter.Convert(10, Units.MILLILITRE, Units.LITRE));
            System.out.println();
            
            System.out.println("Millilitres to US Gallons");
                //test 0 points
            System.out.println("Input 0 Millilitres, should result in 0US gal: " + VolumeConverter.Convert(0, Units.MILLILITRE, Units.US_GALLON));
                //test non 0 point number
            System.out.println("Input 10 Millilitres, should result in 0.00264172US gal: " + VolumeConverter.Convert(10, Units.MILLILITRE, Units.US_GALLON));
            System.out.println();
            
            System.out.println("US Gallons to Litres");
                //test 0 points
            System.out.println("Input 0 US Gallons, should result in 0L: " + VolumeConverter.Convert(0, Units.US_GALLON, Units.LITRE));
                //test non 0 point number
            System.out.println("Input 10 US Gallons, should result in 37.8541L: " + VolumeConverter.Convert(10, Units.US_GALLON, Units.LITRE));
            System.out.println();
            
            System.out.println("Us Gallons to Millilitres");
            //test 0 points
            System.out.println("Input 0 US Gallons, should result in 0mL: " + VolumeConverter.Convert(0, Units.US_GALLON, Units.MILLILITRE));
            //test non 0 point number
            System.out.println("Input 10 US Gallons, should result in 37854.1mL: " + VolumeConverter.Convert(10, Units.US_GALLON, Units.MILLILITRE));
            System.out.println();
        }
        catch(UnitMismatchException e){
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
    
    static void TestMismatchedUnits(){
        System.out.println("Input Kelvin and Litres, should get an error.");
        try{
            Converter.Convert(0, Units.KELVIN, Units.LITRE);
        }
        catch(UnitMismatchException e){
            System.out.println(e.getMessage());
        }
        System.out.println();
    }
}
